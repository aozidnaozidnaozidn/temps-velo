#! /bin/sh -ex

mkdir -p public

minify generated-data/cities.js > public/cities.min.js
minify main.js > public/main.min.js
minify --type html index.html > public/index.html

for fullfile in generated-data/*.data.js; do
		echo $fullfile
		yoyo=$(basename -- "$fullfile")
		filename="${yoyo%%.*}"
		mkdir -p public/"$filename"
		mkdir -p public/"$filename"/static
		cp -r static public/"$filename"/
		minify main.css > public/"$filename"/static/main.min.css
		cat ville.html | awk '{gsub(/Rennes/,"'$filename'")}1' | minify --type html > public/"$filename"/index.html
		minify ville.js > public/"$filename"/static/main.min.js
		minify generated-data/"$filename".data.js > public/"$filename"/static/data.min.js
done
